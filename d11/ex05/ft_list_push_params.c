/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_params.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/27 15:02:42 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/27 15:02:44 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void		ft_list_push_front(t_list **begin_list, void *data)
{
	t_list *new;

	new = ft_create_elem(data);
	new->next = *begin_list;
	*begin_list = new;
}

t_list		*ft_list_push_params(int ac, char **av)
{
	int		i;
	t_list	*new_list;

	i = 1;
	new_list = NULL;
	while (i < ac)
	{
		ft_list_push_front(&new_list, av[i]);
		i++;
	}
	return (new_list);
}
