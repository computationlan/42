/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_clear.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/27 16:00:13 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/27 16:00:15 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

void	ft_list_clear(t_list **begin_list)
{
	t_list	*current;
	t_list	*to_free;

	current = *begin_list;
	to_free = *begin_list;
	while (current)
	{
		to_free = current;
		current = current->next;
		free(to_free);
	}
	*begin_list = NULL;
}
