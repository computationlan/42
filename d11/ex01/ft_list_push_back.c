/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_back.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/27 11:23:16 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/27 11:23:18 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_push_back(t_list **begin_list, void *data)
{
	t_list *current;

	current = *begin_list;
	if (current == 0)
	{
		*begin_list = ft_create_elem(data);
		return ;
	}
	while (current->next != 0)
	{
		current = current->next;
	}
	current->next = ft_create_elem(data);
}
