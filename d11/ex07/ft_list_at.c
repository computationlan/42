/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_at.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/27 17:00:31 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/27 17:00:33 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

t_list	*ft_list_at(t_list *begin_list, unsigned int nbr)
{
	t_list	*current;
	int		i;
	int		limit;

	current = begin_list;
	limit = nbr;
	i = 0;
	while (current)
	{
		if (i == limit)
			return (current);
		current = current->next;
		i++;
	}
	return (0);
}
