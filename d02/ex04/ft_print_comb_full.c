/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_numbers.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/14 16:48:39 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/14 16:48:42 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int ft_putchar(char c)
{
	write(1, &c, 3);
	return(0);
}

void ft_print_comb(void)
{
	int i;
	i = 0;
	while (i < 9)
	{
		ft_putchar(i);
		i = i + 1;
	}

}

int main()
{
	ft_print_comb();
	return(0);
}