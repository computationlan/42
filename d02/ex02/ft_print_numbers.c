/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_numbers.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/14 16:48:39 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/14 16:48:42 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int ft_putchar(char c);

void	ft_print_numbers(void)
{
	char c;
	c = '0';
	while (c <= '9')
	{
		ft_putchar(c);
		c = c + 1;
	}
}