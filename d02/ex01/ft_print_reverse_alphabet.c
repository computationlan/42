/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_reverse_alphabet.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/14 16:25:35 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/14 16:25:41 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int ft_putchar(char c);

void	ft_print_reverse_alphabet(void)
{
	char c;
	c = 'z';
	while (c >= 'a')
	{
		ft_putchar(c);
		c = c - 1;
	}
}