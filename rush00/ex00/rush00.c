/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush00.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/17 16:48:26 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/17 16:48:28 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar(char c);

void	ft_choose_letter(int col, int row, int width, int height)
{
	if (row == 0 || row == height - 1)
	{
		if (col == 0 || col == width - 1)
		{
			ft_putchar('o');
		}
		else
		{
			ft_putchar('-');
		}
	}
	else
	{
		if (col == 0 || col == width - 1)
		{
			ft_putchar('|');
		}
		else
		{
			ft_putchar(' ');
		}
	}
}

void	rush(int width, int height)
{
	int row;
	int col;

	row = 0;
	while (row < height)
	{
		col = 0;
		while (col < width)
		{
			ft_choose_letter(col, row, width, height);
			col = col + 1;
		}
		row = row + 1;
		if (width > 0)
		{
			ft_putchar('\n');
		}
	}
}
