/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_whitespaces.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/22 10:58:55 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/22 10:58:57 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		is_separator(char c)
{
	return (c == ' ' || c == '\t' || c == '\n' || c == '\r'
		|| c == '\v' || c == '\f');
}

int		count_words(char *str)
{
	int i;
	int flag_space;
	int count;

	i = 0;
	flag_space = 1;
	count = 0;
	while (str[i] != 0)
	{
		if (!is_separator(str[i]))
		{
			if (flag_space)
				count++;
			flag_space = 0;
		}
		if (is_separator(str[i]))
			flag_space = 1;
		i++;
	}
	return (count);
}

int		count_letters(char *str)
{
	int i;

	i = 0;
	while ((str[i] != '\0') && !is_separator(str[i]))
	{
		i = i + 1;
	}
	return (i);
}

char	*copy_word(char *dest, char *src)
{
	int i;

	i = 0;
	while ((src[i] != '\0') && !is_separator(src[i]))
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

char	**ft_split_whitespaces(char *str)
{
	int		words;
	char	**result;
	int		word_counter;
	int		word_len;
	int		i;

	words = count_words(str);
	result = (char**)malloc(sizeof(char*) * (words + 1));
	word_counter = 0;
	i = 0;
	while (word_counter < words)
	{
		while (is_separator(str[i]))
			i++;
		word_len = count_letters(&str[i]);
		result[word_counter] = (char*)malloc(sizeof(char) * (word_len + 1));
		copy_word(result[word_counter], &str[i]);
		i += word_len;
		word_counter++;
	}
	result[words] = 0;
	return (result);
}
