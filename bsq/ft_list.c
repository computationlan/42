/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/03 13:02:22 by lnazarov          #+#    #+#             */
/*   Updated: 2016/08/03 13:02:26 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		ft_putchar(str[i]);
		i = i + 1;
	}
}

t_list	*ft_create_elem(char data)
{
	t_list	*node;

	node = malloc(sizeof(t_list));
	node->next = 0;
	node->data = data;
	return (node);
}

void	ft_list_push_back(t_list **begin_list, char data)
{
	t_list *current;

	current = *begin_list;
	if (current == 0)
	{
		*begin_list = ft_create_elem(data);
		return ;
	}
	while (current->next != 0)
	{
		current = current->next;
	}
	current->next = ft_create_elem(data);
}

void	ft_list_clear(t_list **begin_list)
{
	t_list	*current;
	t_list	*to_free;

	current = *begin_list;
	to_free = *begin_list;
	while (current)
	{
		to_free = current;
		current = current->next;
		free(to_free);
	}
	*begin_list = 0;
}
