/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/03 13:27:15 by lnazarov          #+#    #+#             */
/*   Updated: 2016/08/03 13:27:18 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

int		does_it_fit(int *dims, int square, char *map, char *params)
{
	int row;
	int col;

	row = dims[2] / dims[0];
	while (row < (dims[2] / dims[0] + square))
	{
		col = dims[2] % dims[0];
		while (col < (dims[2] % dims[0] + square))
		{
			if ((map[row * dims[0] + col]) == params[1])
				return (0);
			col++;
		}
		row++;
	}
	return (1);
}

void	print_solution(int *dims, int sq, char *map, char *params)
{
	int row;
	int col;
	int	sw[2];
	int	sh[2];

	row = 0;
	sw[0] = dims[2] % dims[0];
	sw[1] = dims[2] % dims[0] + sq;
	sh[0] = dims[2] / dims[0];
	sh[1] = dims[2] / dims[0] + sq;
	while (row < dims[1])
	{
		col = 0;
		while (col < dims[0])
		{
			if (col >= sw[0] && col < sw[1] && row >= sh[0] && row < sh[1])
				ft_putchar(params[2]);
			else
				ft_putchar(map[row * dims[0] + col]);
			col++;
		}
		ft_putchar('\n');
		row++;
	}
}

void	solve(char *map, int h, int w, char *params)
{
	int	sq;
	int	i;
	int dims[3];

	dims[0] = w;
	dims[1] = h;
	sq = ((h < w) ? h : w);
	while (sq >= 0)
	{
		i = 0;
		while (i < h * w)
		{
			if ((i / w <= h - sq) && (i % w <= w - sq))
			{
				dims[2] = i;
				if (does_it_fit(dims, sq, map, params))
				{
					print_solution(dims, sq, map, params);
					return ;
				}
			}
			i++;
		}
		sq--;
	}
}

void	process_file(int fd)
{
	int		height;
	int		width;
	char	params[3];
	char	*map;

	if (read_header(fd, &height, params))
	{
		if ((map = read_map(fd, height, &width, params)))
		{
			solve(map, height, width, params);
			free(map);
			return ;
		}
	}
	ft_putstr("map error\n");
}

int		main(int argc, char **argv)
{
	int		i;
	int		fd;

	i = 1;
	if (argc > 1)
	{
		while (i < argc)
		{
			fd = open(argv[i], O_RDONLY);
			if (fd >= 0)
			{
				process_file(fd);
				close(fd);
			}
			else
				ft_putstr("file error\n");
			i++;
		}
	}
	else
		process_file(0);
	return (0);
}
