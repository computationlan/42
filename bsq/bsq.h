/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bsq.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/03 12:53:08 by lnazarov          #+#    #+#             */
/*   Updated: 2016/08/03 12:53:23 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BSQ_H
# define BSQ_H

# define BUFFER 100
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>

typedef struct	s_list
{
	struct s_list	*next;
	char			data;
}				t_list;

void			ft_putchar(char c);

void			ft_putstr(char *str);

int				ft_atoi_strict(char *s);

t_list			*ft_create_elem(char data);

void			ft_list_push_back(t_list **begin_list, char data);

void			ft_list_clear(t_list **begin_list);

int				read_header(int fd, int *height, char *params);

t_list			*read_to_list(int fd);

int				check_width(t_list *list, int height);

char			*list_to_map(t_list *list, int height, int width, char *params);

char			*read_map(int fd, int height, int *width, char *params);

int				does_it_fit(int *dims, int square, char *map, char *params);

void			print_solution(int *dims, int sq, char *map, char *params);

void			solve(char *map, int h, int w, char *params);

void			process_file(int fd);

#endif
