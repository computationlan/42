/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/03 13:19:51 by lnazarov          #+#    #+#             */
/*   Updated: 2016/08/03 13:19:54 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

int		ft_atoi_strict(char *s)
{
	int i;
	int number;

	i = 0;
	number = 0;
	while (s[i] == ' ' || s[i] == '\t' || (s[i] >= '\n' && s[i] <= '\r'))
		i++;
	if (s[i] == '+')
		i++;
	while (s[i] != '\0')
	{
		if (s[i] < '0' || s[i] > '9')
			return (0);
		number = number * 10 + (s[i] - '0');
		i++;
	}
	return (number);
}

int		read_header(int fd, int *height, char *params)
{
	char	line[BUFFER + 1];
	int		i;

	i = 0;
	while ((i < BUFFER) && read(fd, &line[i], 1) && (line[i] != '\n'))
		i++;
	if ((i < 4) || (line[i] != '\n'))
		return (0);
	params[0] = line[i - 3];
	params[1] = line[i - 2];
	params[2] = line[i - 1];
	line[i - 3] = '\0';
	*height = ft_atoi_strict(line);
	return (height > 0);
}

t_list	*read_to_list(int fd)
{
	t_list	*list;
	char	c;

	list = 0;
	while (read(fd, &c, 1))
		ft_list_push_back(&list, c);
	return (list);
}

char	*read_map(int fd, int height, int *width, char *params)
{
	t_list	*list;
	char	*map;

	list = read_to_list(fd);
	if (!list)
		return (0);
	*width = check_width(list, height);
	if (*width <= 0)
		return (0);
	map = list_to_map(list, height, *width, params);
	ft_list_clear(&list);
	return (map);
}
