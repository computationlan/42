/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/03 13:25:52 by lnazarov          #+#    #+#             */
/*   Updated: 2016/08/03 13:25:54 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

int		check_width(t_list *list, int height)
{
	int		i;
	int		width;
	int		actual_height;

	i = 0;
	width = -1;
	actual_height = 0;
	while (list != 0)
	{
		if (list->data == '\n' || list->next == 0)
		{
			if (width < 0)
				width = i;
			else if (width != i)
				return (-1);
			actual_height++;
			i = 0;
		}
		else
			i++;
		list = list->next;
	}
	if (actual_height != height)
		return (-1);
	return (width);
}

char	*list_to_map(t_list *list, int height, int width, char *params)
{
	char	*map;
	int		i;

	map = (char*)malloc(sizeof(char) * (height * width));
	i = 0;
	while (list)
	{
		if (list->data != '\n')
		{
			if (i >= height * width)
				return (0);
			else if ((list->data == params[0]) || (list->data == params[1]))
				map[i] = list->data;
			else
				return (0);
			i++;
		}
		list = list->next;
	}
	if (i < height * width)
		return (0);
	return (map);
}
