/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_takes_place.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/22 13:06:47 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/22 13:06:49 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	ft_takes_place(int hour)
{
	char *str;

	str = "THE FOLLOWING TAKES PLACE BETWEEN ";
	if (hour == 0 || hour == 24)
		printf("%s12.00 A.M. AND 1.00 A.M.\n", str);
	if (hour == 12)
		printf("%s12.00 P.M. AND 1.00 P.M.\n", str);
	if (hour == 11)
		printf("%s11.00 A.M. AND 12.00 P.M.\n", str);
	if (hour == 23)
		printf("%s11.00 P.M. AND 12.00 A.M.\n", str);
	if (hour > 0 && hour < 11)
		printf("%s%d.00 A.M. AND %d.00 A.M.\n", str, hour, hour + 1);
	if (hour > 12 && hour < 23)
		printf("%s%d.00 P.M. AND %d.00 P.M.\n", str, hour - 12, hour - 11);
}
