/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_concat_params.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/21 15:15:29 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/21 15:15:31 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		i = i + 1;
	}
	return (i);
}

char	*ft_helper(char *str1, char *str2)
{
	int i;
	int end;

	i = 0;
	end = ft_strlen(str1);
	while (str2[i] != '\0')
	{
		str1[end + i] = str2[i];
		i++;
	}
	str1[end + i] = '\n';
	return (str1);
}

char	*ft_concat_params(int argc, char **argv)
{
	int		i;
	int		size;
	char	*new;

	size = 0;
	i = 1;
	while (i < argc)
	{
		size = size + ft_strlen(argv[i]) + 1;
		i++;
	}
	new = (char *)malloc(size);
	i = 0;
	while (i < size)
	{
		new[i++] = 0;
	}
	i = 1;
	while (i < argc)
	{
		new = ft_helper(new, argv[i]);
		i++;
	}
	new[size - 1] = '\0';
	return (new);
}
