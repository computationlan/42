/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/21 10:28:40 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/21 10:28:43 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	*ft_range(int min, int max)
{
	int *new;
	int i;
	int	number;

	if (min >= max)
		return (0);
	new = (int *)malloc(sizeof(int) * (max - min));
	i = 0;
	number = min;
	while (number < max)
	{
		new[i] = number;
		i++;
		number++;
	}
	return (new);
}
