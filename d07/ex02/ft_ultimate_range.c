/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/21 12:50:13 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/21 12:50:15 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	ft_ultimate_range(int **range, int min, int max)
{
	int i;
	int	number;

	if (min >= max)
		return (0);
	*range = (int *)malloc(sizeof(int) * (max - min));
	i = 0;
	number = min;
	while (number < max)
	{
		(*range)[i] = number;
		i++;
		number++;
	}
	return (i);
}
