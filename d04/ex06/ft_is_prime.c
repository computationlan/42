/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_prime.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/16 15:43:01 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/16 15:43:03 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_is_prime(int nb)
{
	int divisor;

	if (nb <= 1)
	{
		return (0);
	}
	divisor = 2;
	while (divisor * divisor <= nb)
	{
		if (nb % divisor == 0)
		{
			return (0);
		}
		divisor = divisor + 1;
	}
	return (1);
}
