/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/16 11:38:59 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/16 11:39:06 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_iterative_factorial(int nb)
{
	int fact;
	int i;

	if (nb < 0 || nb > 12)
	{
		return (0);
	}
	fact = 1;
	i = 1;
	while (i <= nb)
	{
		fact = fact * i;
		i = i + 1;
	}
	return (fact);
}
