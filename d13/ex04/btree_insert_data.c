/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_insert_data.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/29 12:39:40 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/29 12:39:42 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	btree_insert_data(t_btree **root, void *item,
		int (*cmpf)(void *, void *))
{
	int		check;
	t_btree *current;
	t_btree *previous;

	current = *root;
	if (current == 0)
	{
		*root = btree_create_node(item);
		return ;
	}
	check = cmpf(item, current->item);
	while (current)
	{
		current = (check < 0) ? current->left : current->right;
		if (current)
		{
			check = cmpf(item, current->item);
			previous = current;
		}
	}
	if (check < 0)
		previous->left = btree_create_node(item);
	else
		previous->right = btree_create_node(item);
}
