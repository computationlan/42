# These programs were written in C as assignments for the 42 programming bootcamp (42, Fremont, CA)

Assignment descriptions can be found in .pdf files in corresponding folders:

* The folders starting with the letter 'd' are smaller practice problems, many of them are reimplementations of C standard library methods.
* The folders starting with the word 'rush' are from groups assignments. 
* 'Sastantua' and 'Match/n-match' are larger and more challenging weekend projects. 
* 'BSQ' is a final group project. 