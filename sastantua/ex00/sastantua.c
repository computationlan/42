/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sastantua.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/17 17:28:28 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/17 17:28:31 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar(char c);

void	pyramid(int space, int star, int door, char middle)
{
	int i;

	i = 0;
	while (i < space)
	{
		ft_putchar(' ');
		i++;
	}
	ft_putchar('/');
	i = 0;
	while (i < star)
	{
		ft_putchar('*');
		i++;
	}
	i = 0;
	while (i < door)
	{
		ft_putchar(middle);
		i++;
	}
	i = 0;
	while (i < star)
	{
		ft_putchar('*');
		i++;
	}
	ft_putchar('\\');
	ft_putchar('\n');
}

void	level(int floors, int base, int offset)
{
	int i;

	i = 0;
	while (i < floors)
	{
		pyramid((floors - 1 - i + offset), (base - 1) / 2 - floors + i, 1, '*');
		i++;
	}
}

int		base(int level)
{
	int result;
	int i;

	i = 0;
	result = 7;
	while (i < level)
	{
		result += (level - i + 3) * 2 + (level - i + 3) / 2 * 2;
		i++;
	}
	return (result);
}

void	sastantua(int levels)
{
	int i;
	int bottom;

	i = 0;
	bottom = base(levels - 1);
	while (i < levels)
	{
		level(3 + i, base(i), (bottom - base(i)) / 2);
		i++;
	}
}
