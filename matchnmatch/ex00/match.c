/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   match.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/24 18:38:40 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/24 18:38:42 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	nmatch(char *s1, char *s2)
{
	int match_n;
	int i;

	match_n = 0;
	if (s1[0] == '\0' && s2[0] == '\0')
		return (1);
	if (s1[0] == s2[0])
		return (match_n + nmatch(&s1[1], &s2[1]));
	if (s2[0] == '*')
	{
		i = 0;
		while (s1[i] != '\0')
		{
			match_n = match_n + nmatch(&s1[i], &s2[1]);
			i++;
		}
		return (match_n + nmatch(&s1[i], &s2[1]));
	}
	return (match_n);
}

int	match(char *s1, char *s2)
{
	int result;

	result = nmatch(&s1[0], &s2[0]);
	if (result > 0)
		return (1);
	else
		return (0);
}
