/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/19 14:19:23 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/19 14:19:25 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strncpy(char *dest, char *src, unsigned int n)
{
	int i;
	int limit;
	int flag;

	limit = n;
	i = 0;
	flag = 0;
	while (i < limit)
	{
		dest[i] = src[i];
		if (src[i] == '\0')
		{
			flag = 1;
		}
		if (flag == 1)
		{
			dest[i] = '\0';
		}
		i++;
	}
	return (dest);
}
