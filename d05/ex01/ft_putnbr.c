/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lnazarov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/18 10:24:06 by lnazarov          #+#    #+#             */
/*   Updated: 2016/07/18 10:24:08 by lnazarov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar(char c);

int		ft_recursive_help(int nb)
{
	int i;

	if (nb == 0)
	{
		return (0);
	}
	else
	{
		i = nb % 10 + 48;
		ft_recursive_help(nb / 10);
		ft_putchar(i);
	}
	return (0);
}

void	ft_putnbr(int nb)
{
	if (nb == 0)
	{
		ft_putchar('0');
	}
	if (nb < 0)
	{
		ft_putchar('-');
		ft_recursive_help(0 - nb);
	}
	else
	{
		ft_recursive_help(nb);
	}
}
