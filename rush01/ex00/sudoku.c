/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sudoku.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: uramage <uramage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/18 18:23:42 by uramage           #+#    #+#             */
/*   Updated: 2016/07/24 20:57:33 by uramage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		is_row_available(int k, int tab[9][9], int row);
int		is_col_available(int k, int tab[9][9], int col);
int		solve_sudoku(int tab[9][9], int position);

int		is_row_available(int k, int tab[9][9], int row)
{
	int		col;

	col = 0;
	while (col < 9)
	{
		if (tab[row][col] == k)
			return (0);
		col++;
	}
	return (1);
}

int		is_col_available(int k, int tab[9][9], int col)
{
	int		row;

	row = 0;
	while (row < 9)
	{
		if (tab[row][col] == k)
			return (0);
		row++;
	}
	return (1);
}

int		is_block_available(int k, int tab[9][9], int row, int col)
{
	int		s_row;
	int		s_col;

	s_row = row - (row % 3);
	s_col = col - (col % 3);
	row = s_row;
	while (row < s_row + 3)
	{
		col = s_col;
		while (col < s_col + 3)
		{
			if (tab[row][col] == k)
				return (0);
			col++;
		}
		row++;
	}
	return (1);
}

int		solve_sudoku(int tab[9][9], int position)
{
	int		row;
	int		col;
	int		k;

	if (position == 9 * 9)
		return (1);
	row = position / 9;
	col = position % 9;
	if (tab[row][col] != 0)
		return (solve_sudoku(tab, position + 1));
	k = 1;
	while (k <= 9)
	{
		if (is_row_available(k, tab, row)
			&& is_col_available(k, tab, col)
			&& is_block_available(k, tab, row, col))
		{
			tab[row][col] = k;
			if (solve_sudoku(tab, position + 1))
				return (1);
		}
		k++;
	}
	tab[row][col] = 0;
	return (0);
}

int		string_to_sudoku(char *string, int dest[9])
{
	int		i;

	i = 0;
	while (i < 9)
	{
		if (string[i] == '.')
			dest[i] = 0;
		else if ('1' <= string[i] && string[i] <= '9')
			dest[i] = string[i] - '0';
		else
			return (0);
		i++;
	}
	return (1);
}
