/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: uramage <uramage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/18 18:23:42 by uramage           #+#    #+#             */
/*   Updated: 2016/07/24 20:54:41 by uramage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void		ft_putstr(char *str);
int			ft_putchar(char c);
void		print_error(void);
void		print_tab(int tab[9][9]);
int			solve_sudoku(int tab[9][9], int position);
int			string_to_sudoku(char *string, int dest[9]);

int			ft_putchar(char c)
{
	write(1, &c, 1);
	return (0);
}

void		ft_putstr(char *str)
{
	int		i;

	i = 0;
	while (str[i] != '\0')
	{
		ft_putchar(str[i]);
		i++;
	}
}

void		print_error(void)
{
	ft_putstr("Error\n");
}

void		print_tab(int tab[9][9])
{
	int		row;

	row = 0;
	while (row < 9)
	{
		col = 0;
		while (col < 9)
		{
			ft_putchar(tab[row][col] + '0');
			if (col < 9 - 1)
				ft_putchar(' ');
			col++;
		}
		ft_putchar('\n');
		row++;
	}
}

int			main(int argc, char *argv[])
{
	int		tab[9][9];
	int		row;
	int		col;

	if (argc > 1)
	{
		row = 0;
		while (row < 9 && string_to_sudoku(argv[row + 1], tab[row]))
			row++;
		if (row < 9)
			print_error();
		else if (solve_sudoku(tab, 0))
		{
			print_tab(tab);
		}
		else
			print_error();
	}
	else
		print_error();
	return (0);
}
